Insta-Like Web App
==================

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/59e8c92f-a9b0-4dd2-8f2c-ec155bd2f1ae/big.png)](https://insight.sensiolabs.com/projects/59e8c92f-a9b0-4dd2-8f2c-ec155bd2f1ae)

Installation
------------

First, clone from official repo:

`git clone git@bitbucket.org:jaltamir/insta-like-web-app.git`

After that you have to install dependencies and setup the database. Information about db config in [app/config/parameters.yml](app/config/parameters.yml) (see [app/config/config_dev.yml](app/config/config_dev.yml) and [app/config/config_test.yml](app/config/config_test.yml) for dev & test environments).

Run `composer install` for dependencies installation and `php bin/console assets:install --symlink` for installing assets. 

The environments available are prod, dev and test. All are sqlite based, but that can be changed within config files. 

For cleaning data and load initial data needed, run `php bin/console doctrine:fixtures:load --env=<ENVIRONMENT>`

API
---

Documentation is available on route `<url_app>/api` .


Testing
-------

PHPUnit is not included. After install, run ` phpunit.phar -c` (from root dir).

