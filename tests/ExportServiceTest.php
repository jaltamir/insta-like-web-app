<?php

    namespace tests\AppBundle;

    use AppBundle\Entity\Thread;
    use AppBundle\Services\ExportService;
    use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

    class ExportServiceTest extends KernelTestCase
    {
        /**
         * @var \Doctrine\ORM\EntityManager
         */
        private $em;

        /**
         * @var ExportService
         */
        private $export_service;

        /**
         * {@inheritDoc}
         */
        protected function setUp()
        {
            self::bootKernel();

            $this->export_service = static::$kernel->getContainer()->get('app.export_service');

            $this->em = static::$kernel->getContainer()
              ->get('doctrine')
              ->getManager();
        }

        /**
         * {@inheritDoc}
         */
        protected function tearDown()
        {
            parent::tearDown();

            $this->em->close();
            $this->em = null;
        }

        public function testInitialDataLoaded()
        {
            $thread = $this->em
              ->getRepository(Thread::class)
              ->getMainThread()
            ;

            $this->assertTrue($thread instanceof Thread);
        }

        public function testCreateZipFile()
        {
            $thread = $this->em
              ->getRepository(Thread::class)
              ->getMainThread()
            ;

            $zip_name = $this->export_service->createZipFile($thread);
            $zip = new \ZipArchive();
            $this->assertTrue($zip->open($zip_name));
        }

    }