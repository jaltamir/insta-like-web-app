
$( document ).ready(function() {

    var $thread_views = $('#views-counter');

    setInterval(function(){
        $.ajax({
            type: "GET",
            url: $thread_views.attr('data-url'),
            success: function(result) {
                $thread_views.html(result.views);
            }
        });
    }, 15000);

});

