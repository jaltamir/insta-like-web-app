<?php

namespace AppBundle\Form\DataTransformer;

use AppBundle\Entity\Thread;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ThreadToNumberTransformer implements DataTransformerInterface
{
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Transforms an object (thread) to a string (number).
     *
     * @param Thread|null $issue
     *
     * @return string
     */
    public function transform($thread)
    {
        if ($thread instanceof Thread) {
            return $thread->getId();
        }

        return '1';
    }

    /**
     * Transforms a string (number) to an object (thread).
     *
     * @param string $thread_number
     *
     * @return Thread|null
     *
     * @throws TransformationFailedException if object (thread) is not found
     */
    public function reverseTransform($thread_number)
    {
        if (!$thread_number) {
            return;
        }

        $thread = $this->manager
          ->getRepository(Thread::class)
          ->getMainThread()
        ;

        if (!$thread instanceof Thread) {
            throw new TransformationFailedException('The main thread cannot be loaded');
        }

        return $thread;
    }
}
