<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Post;
use AppBundle\Form\DataTransformer\ThreadToNumberTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('title', TextType::class, [
              'label' => 'Title',
              'required' => false,
          ])
          ->add('thread', HiddenType::class, [
              'data' => '1',
          ])
          ->add('image_file', VichImageType::class, [
              'required' => true,
          ]);

        $builder->get('thread')
          ->addModelTransformer(new ThreadToNumberTransformer($options['entity_manager']));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
            'allow_extra_fields' => true,
        ]);

        $resolver->setRequired('entity_manager');
    }
}
