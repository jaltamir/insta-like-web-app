<?php

    namespace AppBundle\Events;

    use AppBundle\Entity\Thread;
    use Symfony\Component\EventDispatcher\Event;

    class ThreadViewEvent extends Event
    {
        const THREAD_VIEW_EVENT = 'thread.view.event';

        /**
         * @var Thread
         */
        private $thread;

        /**
         * ThreadViewEvent constructor.
         * @param Thread $thread
         */
        public function __construct(Thread $thread)
        {
            $this->thread = $thread;
        }

        /**
         * @return Thread
         */
        public function getThread()
        {
            return $this->thread;
        }

    }

