<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Vich\Uploadable
 * @ApiResource
 */
class Post
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Type(type="string", message="post.title.type")
     *
     * @var string
     */
    private $title;

    /**
     * @Assert\Image(
     *     maxSize="2M",
     *     maxSizeMessage="Size allowed: up to 2M",
     *     maxWidth="1920",
     *     maxWidthMessage="The max width is 1920px",
     *     maxHeight="1080",
     *     maxHeightMessage="The max height is 1080px",
     *     mimeTypes = {"image/jpeg", "image/png", "image/gif"},
     *     mimeTypesMessage = "Please upload a valid image. The supported formats are JPEG, PNG and GIF"
     * )
     * @Vich\UploadableField(mapping="post_image", fileNameProperty="image_name")
     *
     * @var File
     */
    private $image_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $image_name;

    /**
     * Many Features have One Product.
     *
     * @ORM\ManyToOne(targetEntity="Thread", inversedBy="posts")
     * @ORM\JoinColumn(name="thread_id", referencedColumnName="id")
     */
    private $thread;

    /**
     * @ORM\Column(name="created_datetime",type="datetime")
     *
     * @var \DateTime
     */
    private $created_datetime;

    /**
     * @ORM\Column(name="updated_datetime", type="datetime")
     *
     * @var \DateTime
     */
    private $updated_datetime;

    public function __construct()
    {
        $this->created_datetime = new \DateTime('now');
        $this->updated_datetime = new \DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * @param Thread $thred
     *
     * @return $this
     */
    public function setThread(Thread $thred)
    {
        $this->thread = $thred;

        return $this;
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_datetime = new \DateTime('now');
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedDatetime()
    {
        return $this->updated_datetime;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDatetime()
    {
        return $this->created_datetime;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setTitle($value)
    {
        $this->title = $value;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return $this
     */
    public function setImageFile(File $image = null)
    {
        $this->image_file = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updated_datetime = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->image_file;
    }

    /**
     * @param string $image_name
     *
     * @return $this
     */
    public function setImageName($image_name)
    {
        $this->image_name = $image_name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName()
    {
        return $this->image_name;
    }
}
