<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Table(indexes={@ORM\Index(name="views_idx", columns={"views"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ThreadRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @ApiResource
 */
class Thread
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\Type(type="integer", message="thread.views.type")
     *
     * @var int
     */
    private $views;

    /**
     * @ORM\OneToMany(targetEntity="Post", mappedBy="thread")
     *
     * @var ArrayCollection
     */
    private $posts;

    /**
     * @ORM\Column(name="created_datetime",type="datetime")
     *
     * @var \DateTime
     */
    private $created_datetime;

    /**
     * @ORM\Column(name="updated_datetime", type="datetime")
     *
     * @var \DateTime
     */
    private $updated_datetime;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
        $this->created_datetime = new \DateTime('now');
        $this->updated_datetime = new \DateTime('now');
        $this->views = 0;
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_datetime = new \DateTime('now');
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedDatetime()
    {
        return $this->updated_datetime;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDatetime()
    {
        return $this->created_datetime;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @return $this
     */
    public function increaseView()
    {
        ++$this->views;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param Post $post
     *
     * @return $this
     */
    public function addPost(Post $post)
    {
        if (!$this->posts->contains($post)) {
            $this->posts->add($post);
        }

        return $this;
    }
}
