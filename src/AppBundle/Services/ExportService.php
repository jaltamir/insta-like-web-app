<?php

namespace AppBundle\Services;

use AppBundle\Entity\Post;
use AppBundle\Entity\Thread;
use Liuggio\ExcelBundle\Factory;

class ExportService
{
    /**
     * @var Factory
     */
    private $factory;

    /**
     * @var string
     */
    private $tmp_dir;

    public function __construct(Factory $factory, $tmp_dir)
    {
        $this->factory = $factory;
        $this->tmp_dir = $tmp_dir;
    }

    /**
     * @param Thread $thread
     *
     * @return string zip_name
     */
    public function createZipFile(Thread $thread)
    {
        $zip = new \ZipArchive();
        $zip_name = $this->tmp_dir.'/thread_export ['.date('Y_m_d_H_i_s').'].zip';
        $zip->open($zip_name, \ZipArchive::CREATE);

        foreach ($thread->getPosts() as $post) {
            /* @var Post $post */
            $zip->addFromString($post->getId().'_'.$post->getImageName(), file_get_contents($post->getImageFile()));
        }

        $zip->addFromString('thread summary.xls', $this->getExcelSummaryAsString($thread));
        $zip_name = $zip->filename;
        $zip->close();

        return $zip_name;
    }

    /**
     * @param Thread $thread
     *
     * @return string
     */
    private function getExcelSummaryAsString(Thread $thread)
    {
        $writer = $this->factory->createWriter($this->getExcelSummary($thread));
        ob_start();
        $writer->save('php://output');
        $excel_as_string = ob_get_clean();

        return $excel_as_string;
    }

    /**
     * @param Thread $thread
     *
     * @return \PHPExcel
     *
     * @throws \PHPExcel_Exception
     */
    private function getExcelSummary(Thread $thread)
    {
        $php_excel = $this->factory->createPHPExcelObject();

        $php_excel->setActiveSheetIndex(0);
        $sheet = $php_excel->getActiveSheet();

        $sheet->setTitle('Thread summary')
          ->setCellValue('A1', 'Title')
          ->setCellValue('B1', 'Filename');

        $row_index = 2;
        foreach ($thread->getPosts() as $post) {
            /* @var Post $post */
            $sheet->setCellValue('A'.$row_index, (string) $post->getTitle())
              ->setCellValue('B'.$row_index, $post->getImageName());
            ++$row_index;
        }

        return $php_excel;
    }
}
