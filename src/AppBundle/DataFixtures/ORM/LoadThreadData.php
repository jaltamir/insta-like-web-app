<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Thread;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadThreadData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $manager->persist(new Thread());
        $manager->flush();
    }
}
