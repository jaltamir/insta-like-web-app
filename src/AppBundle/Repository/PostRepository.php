<?php

namespace AppBundle\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class PostRepository extends EntityRepository
{
    /**
     * @return ArrayCollection
     */
    public function getMainThreadOrderedPosts()
    {
        $alias = 'post';

        return $this->createQueryBuilder($alias)
          ->select($alias)
          ->where((new Query\Expr())->eq("$alias.thread", ':main_thread'))
          ->setParameter(':main_thread', 1)
          ->orderBy("$alias.id", 'desc')
          ->getQuery()
          ->getResult();
    }
}
