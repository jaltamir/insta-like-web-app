<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Thread;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class ThreadRepository extends EntityRepository
{
    /**
     * @return Thread
     */
    public function getMainThread()
    {
        $alias = 'thread';

        return $this->createQueryBuilder($alias)
          ->select($alias)
          ->where((new Query\Expr())->eq("$alias.id", ':main_thread'))
          ->setParameter(':main_thread', 1)
          ->getQuery()
          ->getSingleResult();
    }
}
