<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Thread;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ThreadController extends Controller
{
    /**
     * @Route("/thread/export/{id}", requirements={"id": "\d+"}, name="export_thread")
     * @Method("GET")
     *
     * @param Thread $thread
     *
     * @return Response
     *
     * @throws InvalidArgumentException
     */
    public function exportThreadAction(Thread $thread)
    {
        $zip_name = $this->get('app.export_service')->createZipFile($thread);

        $response = new Response(file_get_contents($zip_name));
        $response->headers->set(
          'Content-Disposition',
          $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'thread_export ['.$thread->getId().'].zip')
        );
        $response->headers->set('Content-Type', 'application/zip');

        return $response;
    }
}
