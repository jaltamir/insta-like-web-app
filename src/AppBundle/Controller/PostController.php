<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\Thread;
use AppBundle\Events\ThreadViewEvent;
use AppBundle\Form\Type\PostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PostController extends Controller
{
    /**
     * @Route("/", name="thread_view")
     * @Method("GET")
     */
    public function threadViewAction(Request $request)
    {
        $thread = $this->getDoctrine()->getRepository(Thread::class)->getMainThread();

        $event = new ThreadViewEvent($thread);

        $this->get('event_dispatcher')->dispatch(ThreadViewEvent::THREAD_VIEW_EVENT,$event);

        $form = $this->createForm(PostType::class, new Post(), [
          'method' => 'POST',
          'action' => $this->generateUrl('new_post'),
          'entity_manager' => $this->getDoctrine()->getManager(),
        ])
          ->add($this->get('translator')->trans('Upload image'), SubmitType::class)
        ;

        return $this->render('@App/app/thread_view.html.twig', [
          'thread' => $thread,
          'posts' => $this->getDoctrine()->getRepository(Post::class)->getMainThreadOrderedPosts(),
          'post_form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/post/new", name="new_post")
     * @Method("POST")
     */
    public function newPostAction(Request $request)
    {
        $post = new Post();

        $form = $this->createForm(PostType::class, $post, [
          'method' => 'POST',
          'action' => $this->generateUrl('new_post'),
          'entity_manager' => $this->getDoctrine()->getManager(),
        ])
          ->add($this->get('translator')->trans('Save'), SubmitType::class)
        ;
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('The post has been created'));

            return $this->redirectToRoute('thread_view');
        }

        return $this->render('@App/app/thread_view.html.twig', [
          'thread' => $this->getDoctrine()->getRepository(Thread::class)->getMainThread(),
          'posts' => $this->getDoctrine()->getRepository(Post::class)->getMainThreadOrderedPosts(),
          'post_form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/image/post/{id}", requirements={"id": "\d+"}, name="load_post_image")
     * @Method("GET")
     *
     * @param Post $post
     *
     * @return Response
     *
     * @throws InvalidArgumentException
     */
    public function loadPostImageAction(Post $post)
    {
        return $this->get('vich_uploader.download_handler')->downloadObject($post, 'image_file');
    }
}
