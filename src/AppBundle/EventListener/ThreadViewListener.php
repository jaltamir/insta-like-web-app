<?php

    namespace AppBundle\EventListener;

    use AppBundle\Events\ThreadViewEvent;
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\EventDispatcher\EventSubscriberInterface;

    class ThreadViewListener implements EventSubscriberInterface
    {
        /**
         * @var EntityManagerInterface
         */
        private $em;

        /**
         * ThreadViewListener constructor.
         * @param EntityManagerInterface $em
         */
        public function __construct(EntityManagerInterface $em)
        {
            $this->em = $em;
        }

        /**
         * @param ThreadViewEvent $event
         */
        public function handler(ThreadViewEvent $event)
        {
            $this->em->persist($event->getThread()->increaseView());
            $this->em->flush();
        }

        /**
         * @return array
         */
        public static function getSubscribedEvents()
        {
            return [ThreadViewEvent::THREAD_VIEW_EVENT => 'handler'];
        }
    }

